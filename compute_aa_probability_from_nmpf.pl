#!/usr/bin/perl

$RF=1;
if (@ARGV==0){print "$0 [nmpf file] [Reading frame (1|2|3)]\n"; exit(1);}
if (@ARGV[1]){$RF=$ARGV[1];}

%codon_map=(          "TTT"=>"F",
	 "TCT"=>"S",
	 "TAT"=>"Y",
	 "TGT"=>"C",
	 "TTC"=>"F",
	 "TCC"=>"S",
	 "TAC"=>"Y",
	 "TGC"=>"C",
	 "TTA"=>"L",
	 "TCA"=>"S",
	 "TAA"=>"*",
	 "TGA"=>"*",
	 "TTG"=>"L",
	 "TCG"=>"S",
	 "TAG"=>"*",
	 "TGG"=>"W",
	 "CTT"=>"L",
	 "CCT"=>"P",
	 "CAT"=>"H",
	 "CGT"=>"R",
	 "CTC"=>"L",
	 "CCC"=>"P",
	 "CAC"=>"H",
	 "CGC"=>"R",
	 "CTA"=>"L",
	 "CCA"=>"P",
	 "CAA"=>"Q",
	 "CGA"=>"R",
	 "CTG"=>"L",
	 "CCG"=>"P",
	 "CAG"=>"Q",
	 "CGG"=>"R",
	 "ATT"=>"I",
	 "ACT"=>"T",
	 "AAT"=>"N",
	 "AGT"=>"S",
	 "ATC"=>"I",
	 "ACC"=>"T",
	 "AAC"=>"N",
	 "AGC"=>"S",
	 "ATA"=>"I",
	 "ACA"=>"T",
	 "AAA"=>"K",
	 "AGA"=>"R",
	 "ATG"=>"M",
	 "ACG"=>"T",
	 "AAG"=>"K",
	 "AGG"=>"R",
	 "GTT"=>"V",
	 "GCT"=>"A",
	 "GAT"=>"D",
	 "GGT"=>"G",
	 "GTC"=>"V",
	 "GCC"=>"A",
	 "GAC"=>"D",
	 "GGC"=>"G",
	 "GTA"=>"V",
	 "GCA"=>"A",
	 "GAA"=>"E",
	 "GGA"=>"G",
	 "GTG"=>"V",
	 "GCG"=>"A",
	 "GAG"=>"E",
	 "GGG"=>"G",
	 "GCN"=>"A",
	 "CGN"=>"R",
	 "MGR"=>"R",
	 "AAY"=>"N",
	 "GAY"=>"D",
	 "TGY"=>"C",
	 "CAR"=>"Q",
	 "GAR"=>"E",
	 "GGN"=>"G",
	 "CAY"=>"H",
	 "ATH"=>"I",
	 "YTR"=>"L",
	 "CTN"=>"L",
	 "AAR"=>"K",
	 "TTY"=>"F",
	 "CCN"=>"P",
	 "TCN"=>"S",
	 "AGY"=>"S",
	 "ACN"=>"T",
	 "TAY"=>"Y",
	 "GTN"=>"V",
	 "TAR"=>"*",
	 "TRA"=>"*",
	 );

open(FILE,$ARGV[0]);
@lines=<FILE>;
close(FILE);

chomp $lines[1];
@data=split(/\t/, $lines[1]);
for($i=1; $i<@data; $i++)
{
    $prob_hash{$i}{"A"}=$data[$i];
}
chomp $lines[2];
@data=split(/\t/, $lines[2]);
for($i=1; $i<@data; $i++)
{
    $prob_hash{$i}{"G"}=$data[$i];
}
chomp $lines[3];
@data=split(/\t/, $lines[3]);
for($i=1; $i<@data; $i++)
{
    $prob_hash{$i}{"T"}=$data[$i];
}
chomp $lines[4];
@data=split(/\t/, $lines[4]);
for($i=1; $i<@data; $i++)
{
    $prob_hash{$i}{"C"}=$data[$i];
}
chomp $lines[5];
@data=split(/\t/, $lines[5]);
for($i=1; $i<@data; $i++)
{
    $prob_hash{$i}{"-"}=$data[$i];
}


@bases=qw(A T C G);

for($i=$RF; $i<@data; $i+=3)
{
    $aa_pos=($i+(3-$RF))/3;
    print "$aa_pos\n"; 
    %amino_acid_prob=();
    foreach $pos1 (@bases)
    {
	foreach $pos2 (@bases)
	{
	    foreach $pos3 (@bases)
	    {
		$codon=$pos1.$pos2.$pos3;
#		print "$codon\n"; 
		$prob_codon=$prob_hash{$i}{$pos1}*$prob_hash{$i+1}{$pos2}*$prob_hash{$i+2}{$pos3};
		$amino_acid_prob{$codon_map{$codon}}+=$prob_codon;
	    }
	}
    }
    foreach $key (sort {$amino_acid_prob{$b} <=> $amino_acid_prob{$a}} keys(%amino_acid_prob))
    {
	if ($amino_acid_prob{$key}>0.01){
	    print "$key\t$amino_acid_prob{$key}\n"; }
    }
    #<stdin>;
}
